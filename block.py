#!/usr/bin/env python3

from pygtail import Pygtail
import time
import re
import os
from geolite2 import geolite2
import iptc
import argparse
import sqlite3


def blockip(_ip):
    chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), "INPUT")
    rule = iptc.Rule()
    rule.src = _ip
    rule_atr = rule.create_match("comment")
    rule_atr.comment = "blocked by script"
    target = iptc.Target(rule, "DROP")
    rule.target = target
    chain.insert_rule(rule)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test script to block ip')
    parser.add_argument('-p', dest='path', help="path to log file", required=False, type=str)
    args = parser.parse_args()

    if not args.path:
        parser.print_usage()
        exit(1)
    file = args.path
    if not os.path.isfile(file):
        print("The specified file does not exist")
        exit(1)

    if not os.getuid() == 0:
        print("You must be root to change IPTables.")
        exit(1)

    pattern_ip = re.compile('''((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)''')
    pattern_uri = re.compile('''(http|https):\/\/([\w\-_]+(?:(?:\.[\w\-_]+)+))([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?''')

    threshold = 20
    time_interval = 60
    reader = geolite2.reader()
    stop_file = "stop.file"
    connection = sqlite3.connect("tmp.db")
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS logs (ip TEXT, uri TEXT, count INT DEFAULT 1, state BOOL DEFAULT TRUE);")
    cursor.execute("CREATE UNIQUE INDEX IF NOT EXISTS pairs ON logs (ip, uri);")

    print("Monitoring file")

    # read new data from file every minute
    while True:
        # print("---")
        for line in Pygtail(file):
            # search ip in line
            result_ips = re.search(pattern_ip, line)
            result_uri = re.search(pattern_uri, line)
            if result_ips and result_uri:
                # get ip country
                match = reader.get(result_ips[0])
                if match is not None:
                    # check if ip is Russian
                    if match['country']['iso_code'] != 'RU':
                        cursor.execute("INSERT INTO logs (ip,uri) VALUES(?, ?) ON CONFLICT(ip,uri) "
                                       "DO UPDATE SET count=count+1,state=TRUE;",
                                       (result_ips[0], result_uri[0]))
                        connection.commit()
                        # print(result_ips[0] + " - " + result_uri[0])
        rows = cursor.execute("SELECT ip, uri, count FROM logs WHERE state=TRUE AND count>?", [threshold]).fetchall()
        # Check if somebody reach the threshold value
        for eachrow in rows:
            blockip(eachrow[0])
            print("{} has been blocked for queries to {}. Queries/Threshold: {}/{}".format(
                eachrow[0],
                eachrow[1],
                eachrow[2],
                threshold))

        if os.path.isfile(stop_file):
            print("Stop file exists. Stopping the app.")
            exit(1)
        time.sleep(time_interval)
        # rotate data and delete old data
        cursor.execute("DELETE FROM logs WHERE state=FALSE")
        cursor.execute("UPDATE logs SET state=FALSE WHERE state=TRUE;")
        connection.commit()
